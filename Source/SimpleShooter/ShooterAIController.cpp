// Copyright © 2021 Derek Fletcher, All rights reserved

#include "ShooterAIController.h"

#include "ShooterCharacter.h"

#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"

void AShooterAIController::BeginPlay() 
{
  Super::BeginPlay();

  if (AIBehavior)
  {
    RunBehaviorTree(AIBehavior);
    GetBlackboardComponent()->SetValueAsVector(TEXT("StartLocation"), GetPawn()->GetActorLocation());
  }
}

bool AShooterAIController::IsDead() const
{
  if (AShooterCharacter* ControlledCharacter = Cast<AShooterCharacter>(GetPawn()))
  {
    return ControlledCharacter->IsDead();
  }

  return true;
}
