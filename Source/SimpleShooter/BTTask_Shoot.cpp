// Copyright © 2021 Derek Fletcher, All rights reserved

#include "BTTask_Shoot.h"

#include "ShooterCharacter.h"

#include "AIController.h"

UBTTask_Shoot::UBTTask_Shoot() 
{
  NodeName = TEXT("Shoot");
}

EBTNodeResult::Type UBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) 
{
  Super::ExecuteTask(OwnerComp, NodeMemory);

  if (!OwnerComp.GetAIOwner()->GetPawn()) return EBTNodeResult::Failed;

  if (AShooterCharacter* Character = Cast<AShooterCharacter>(OwnerComp.GetAIOwner()->GetPawn())) {
    Character->PrimaryFire();
    return EBTNodeResult::Succeeded;
  }

  return EBTNodeResult::Failed;
}