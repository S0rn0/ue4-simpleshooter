// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "ShooterGameModeBase.h"
#include "KillAllEnemiesGameMode.generated.h"

UCLASS()
class SIMPLESHOOTER_API AKillAllEnemiesGameMode : public AShooterGameModeBase
{
	GENERATED_BODY()

public:

  virtual void PawnKilled(APawn* PawnKilled) override;
	
private:

  void EndGame(bool bIsPlayerWinner);
};
