// Copyright © 2021 Derek Fletcher, All rights reserved

#include "ShooterCharacter.h"

#include "Components/CapsuleComponent.h"

#include "Gun.h"
#include "ShooterGameModeBase.h"

// Sets default values
AShooterCharacter::AShooterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	
  GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);

  FActorSpawnParameters SpawnParams;
  SpawnParams.Owner = this;
  SpawnParams.Instigator = this;

  Gun = GetWorld()->SpawnActor<AGun>(GunClass, SpawnParams);
  Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponSocket"));

  Health = MaxHealth;
}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

  PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterCharacter::MoveForward);
  PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterCharacter::MoveRight);
  PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
  PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);

  PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AShooterCharacter::LookUpRate);
  PlayerInputComponent->BindAxis(TEXT("LookRightRate"), this, &AShooterCharacter::LookRightRate);

  PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ACharacter::Jump);
  PlayerInputComponent->BindAction(TEXT("PrimaryFire"), IE_Pressed, this, &AShooterCharacter::PrimaryFire);
}

float AShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) 
{
  float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

  DamageToApply = FMath::Min(Health, DamageToApply);
  Health -= DamageToApply;

  if (IsDead())
  {
    AShooterGameModeBase* GM = GetWorld()->GetAuthGameMode<AShooterGameModeBase>();
    if (GM)
    {
      GM->PawnKilled(this);
    }

    DetachFromControllerPendingDestroy();
    GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
  }

  return DamageToApply;
}

bool AShooterCharacter::IsDead() const
{
  return Health <= 0.f;
}

float AShooterCharacter::GetHealthPercent() const
{
  return Health / MaxHealth;
}

void AShooterCharacter::MoveForward(float AxisValue) 
{
  AddMovementInput(GetActorForwardVector() * AxisValue);
}

void AShooterCharacter::MoveRight(float AxisValue) 
{
  AddMovementInput(GetActorRightVector() * AxisValue);
}

void AShooterCharacter::LookUpRate(float AxisValue) 
{
  AddControllerPitchInput(AxisValue * RotationRate * GetWorld()->DeltaTimeSeconds);
}

void AShooterCharacter::LookRightRate(float AxisValue) 
{
  AddControllerYawInput(AxisValue * RotationRate * GetWorld()->DeltaTimeSeconds);
}

void AShooterCharacter::PrimaryFire() 
{
  if (Gun) Gun->PrimaryFire();
}
