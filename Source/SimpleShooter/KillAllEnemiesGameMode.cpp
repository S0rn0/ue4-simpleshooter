// Copyright © 2021 Derek Fletcher, All rights reserved

#include "KillAllEnemiesGameMode.h"

#include "ShooterAIController.h"

#include "EngineUtils.h"

void AKillAllEnemiesGameMode::PawnKilled(APawn* PawnKilled) 
{
  Super::PawnKilled(PawnKilled);

  APlayerController* PC = Cast<APlayerController>(PawnKilled->GetController());
  if (PC)
  {
    EndGame(false);
  }

  for (AShooterAIController* AIController : TActorRange<AShooterAIController>(GetWorld()))
  {
    if (!AIController->IsDead())
    {
      return;
    }
  }

  EndGame(true);
  return;
}

void AKillAllEnemiesGameMode::EndGame(bool bIsPlayerWinner) 
{
  for (AController* Controller : TActorRange<AController>(GetWorld()))
  {
    Controller->GameHasEnded(Controller->GetPawn(), Controller->IsPlayerController() == bIsPlayerWinner);
  }
}
