// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ShooterPlayerController.generated.h"

class UUserWidget;

UCLASS()
class SIMPLESHOOTER_API AShooterPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

  virtual void GameHasEnded(AActor* EndGameFocus, bool bIsWinner) override;

protected:

  virtual void BeginPlay() override;

private:

  UPROPERTY(EditAnywhere)
  TSubclassOf<UUserWidget> LostMessageClass;
  UPROPERTY(EditAnywhere)
  TSubclassOf<UUserWidget> WinMessageClass;
  UPROPERTY(EditAnywhere)
  TSubclassOf<UUserWidget> HUDClass;

  UPROPERTY(EditAnywhere)
  float RestartDelay = 5.f;

  FTimerHandle RestartTimer;

  UPROPERTY()
  UUserWidget* HUD;
};
