// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Services/BTService_BlackboardBase.h"
#include "BTService_SetBlackboardLocation.generated.h"

UCLASS()
class SIMPLESHOOTER_API UBTService_SetBlackboardLocation : public UBTService_BlackboardBase
{
	GENERATED_BODY()
	
public:

  UBTService_SetBlackboardLocation();

protected:

	UPROPERTY(EditAnywhere)
	bool bSetOnlyIfSeen;

  virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
