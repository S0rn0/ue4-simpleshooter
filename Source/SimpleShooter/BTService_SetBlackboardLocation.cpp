// Copyright © 2021 Derek Fletcher, All rights reserved

#include "BTService_SetBlackboardLocation.h"

#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"

UBTService_SetBlackboardLocation::UBTService_SetBlackboardLocation() 
{
  NodeName = TEXT("Set Blackboard Location");
}

void UBTService_SetBlackboardLocation::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) 
{
  Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

  if (APawn* PlayerPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0))
  {
    if (bSetOnlyIfSeen && !OwnerComp.GetAIOwner()->LineOfSightTo(PlayerPawn))
    {
      OwnerComp.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey());
    } else
    {
      OwnerComp.GetBlackboardComponent()->SetValueAsObject(GetSelectedBlackboardKey(), PlayerPawn);
    }
  }
}
