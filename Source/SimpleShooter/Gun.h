// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Gun.generated.h"

class UParticleSystem;

UCLASS()
class SIMPLESHOOTER_API AGun : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGun();

  void PrimaryFire();

private:	

  UPROPERTY(VisibleAnywhere)
  USceneComponent* Root;

  UPROPERTY(VisibleAnywhere)
  USkeletalMeshComponent* Mesh;

  UPROPERTY(EditAnywhere)
  UParticleSystem* MuzzleFlash;

  UPROPERTY(EditAnywhere)
  USoundBase* MuzzleSound;

  UPROPERTY(EditAnywhere)
  UParticleSystem* HitEffect;

  UPROPERTY(EditAnywhere)
  USoundBase* HitSound;

  UPROPERTY(EditAnywhere)
  float MaxRange = 4096;

  UPROPERTY(EditAnywhere)
  float Damage = 10;

  bool GunTrace(FHitResult& Hit, FVector& ShotDirection) const;

  AController* GetOwnerController() const;
};
