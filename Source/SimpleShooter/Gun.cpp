// Copyright © 2021 Derek Fletcher, All rights reserved

#include "Gun.h"

#include "Kismet/GameplayStatics.h"
#include "Components/SkeletalMeshComponent.h"
#include "Particles/ParticleSystem.h"

#include "DrawDebugHelpers.h"

// Sets default values
AGun::AGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

  Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
  RootComponent = Root;

  Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
  Mesh->SetupAttachment(Root);
}

void AGun::PrimaryFire() 
{
  UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("MuzzleFlashSocket"));
  UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("MuzzleFlashSocket"));

  FHitResult Hit;
  FVector ShotDirection;
  if (GunTrace(OUT Hit, OUT ShotDirection))
  {
    UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitEffect, Hit.Location, ShotDirection.Rotation());
    UGameplayStatics::SpawnSoundAtLocation(GetWorld(), HitSound, Hit.Location);

    AController* OwnerController = GetOwnerController();
    if (!OwnerController)
    {
      return;
    }

    AActor* HitActor = Hit.GetActor();
    if (HitActor) {
      HitActor->TakeDamage(Damage, FPointDamageEvent(Damage, Hit, ShotDirection, nullptr), OwnerController, this);
    }
  }
}

bool AGun::GunTrace(FHitResult& Hit, FVector& ShotDirection) const
{
  AController* OwnerController = GetOwnerController();
  if (!OwnerController)
  {
    return false;
  }

  FVector ViewLocation;
  FRotator ViewRotation;
  OwnerController->GetPlayerViewPoint(OUT ViewLocation, OUT ViewRotation);

  FVector End = ViewLocation + ViewRotation.Vector() * MaxRange;

  FCollisionQueryParams Params;
  Params.AddIgnoredActor(this);
  Params.AddIgnoredActor(GetOwner());

  ShotDirection = -ViewRotation.Vector();
  return GetWorld()->LineTraceSingleByChannel(Hit, ViewLocation, End, ECC_GameTraceChannel1, Params);
}

AController* AGun::GetOwnerController() const
{
  APawn* PawnOwner = Cast<APawn>(GetOwner());
  if (!PawnOwner) return nullptr;
  return PawnOwner->GetController();
}

