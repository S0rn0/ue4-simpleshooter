// Copyright © 2021 Derek Fletcher, All rights reserved

#include "ShooterPlayerController.h"

#include "Blueprint/UserWidget.h"

#include "TimerManager.h"

void AShooterPlayerController::BeginPlay() 
{
  Super::BeginPlay();

  HUD = CreateWidget(this, HUDClass, TEXT("HUD"));
  if (HUD)
  {
    HUD->AddToViewport();
  }
}

void AShooterPlayerController::GameHasEnded(AActor* EndGameFocus, bool bIsWinner) 
{
  Super::GameHasEnded(EndGameFocus, bIsWinner);

  HUD->RemoveFromViewport();

  if (bIsWinner)
  {
    if (UUserWidget* WinMessage = CreateWidget(this, WinMessageClass, TEXT("Win Message")))
    {
      WinMessage->AddToViewport();
    }
  }
  else
  {
    if (UUserWidget* LostMessage = CreateWidget(this, LostMessageClass, TEXT("Lost Message")))
    {
      LostMessage->AddToViewport();
    }
  }

  GetWorldTimerManager().SetTimer(RestartTimer, this, &APlayerController::RestartLevel, RestartDelay);
}
