// Copyright © 2021 Derek Fletcher, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "ShooterAIController.generated.h"

class UBehaviorTree;

UCLASS()
class SIMPLESHOOTER_API AShooterAIController : public AAIController
{
	GENERATED_BODY()

public:

  bool IsDead() const;

protected:

  virtual void BeginPlay() override;

private:

  UPROPERTY(EditAnywhere)
  UBehaviorTree* AIBehavior;
};
